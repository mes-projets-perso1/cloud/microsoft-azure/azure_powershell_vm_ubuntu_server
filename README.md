# Script PowerShell pour le déploiement d'une machine virtuelle Azure avec Ansible

Ce script PowerShell est conçu pour automatiser le déploiement d'une machine virtuelle (VM) Azure avec Ansible. Il utilise les cmdlets Azure PowerShell pour créer les ressources nécessaires dans Azure, configurer la machine virtuelle et installer Ansible dessus.

## Variables globales

Le script commence par définir plusieurs variables globales telles que le nom du projet, le groupe de ressources, l'emplacement, etc. Ces variables seront utilisées tout au long du script pour référencer les différentes ressources Azure.

## Création des ressources Azure

Le script crée un groupe de ressources, un réseau virtuel, un sous-réseau, un groupe de sécurité réseau (NSG), une adresse IP publique et une interface réseau pour la machine virtuelle.

## Génération de la clé SSH

Le script vérifie si un dossier `.ssh` existe localement. S'il n'existe pas, il le crée. Ensuite, il génère une paire de clés SSH (privée et publique) et les stocke dans ce dossier.

## Configuration de la machine virtuelle

Le script configure la machine virtuelle en définissant le nom d'utilisateur et le mot de passe, puis crée la configuration de la VM avec l'OS Linux Ubuntu 22.04 LTS. Il ajoute également l'interface réseau à la VM.

## Démarrage de la machine virtuelle

Une fois la machine virtuelle configurée, le script démarre la VM et installe le module `Az.Ssh` pour faciliter l'utilisation des commandes SSH depuis PowerShell.

## Installation d'Ansible sur la VM

Le script utilise une extension personnalisée Azure pour exécuter un script d'installation d'Ansible sur la machine virtuelle. Il vérifie ensuite si l'installation a réussi et affiche la version d'Ansible installée.

## Connexion à la VM

Enfin, le script propose une connexion à la VM en utilisant la clé SSH générée précédemment. S'il n'y a pas de clé SSH, il utilise le mot de passe pour se connecter à la VM.

Ce script permet ainsi de déployer rapidement une machine virtuelle Azure prête à utiliser Ansible pour la configuration et la gestion des infrastructures.
