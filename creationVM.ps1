# Variables globales de la VM Master
$projectName = "TutoAnsible"
$rgName = $projectName + '-rg'
$location = "northeurope"
$vnetname = $projectName + "-vn"
$vnetAddress = "10.0.0.0/16"
$subnetname = $projectName + "-sbn"
$subnetAddress = "10.0.2.0/24";
$masterOSDiskName = $projectName + "master-osdisk"
$masterNICName = $projectName + "master-nic"
$NSGName = $projectName + "-NSG"
$masterVMName = $projectName + "master-vm"
$VMSize = "Standard_B1s"
$PublisherName = "Canonical"
$Offer = "0001-com-ubuntu-server-jammy"
$SKU = "22_04-lts-gen2"
$version = "22.04.202312060"
$dossierSSH = "$env:USERPROFILE/.ssh"

<# # Connexion à Azure (vous serez invité à entrer vos informations d'identification)
Connect-AzAccount #>

# Groupe de ressource
New-AzResourceGroup -Name $rgName -location $location

# Réseau virtuel
$vnet = @{
    Name = $vnetname
    ResourceGroupName = $rgName
    Location = $location
    AddressPrefix = $vnetAddress
    Subnet = $createSubnet
}
$vnet = New-AzVirtualNetwork @vnet 

# Sous-réseaux
$subnet = @{
    Name = $subnetname
    AddressPrefix = $subnetAddress
}

# Ajouter le sous-réseau au réseau virtuel
$vnet | Add-AzVirtualNetworkSubnetConfig @subnet | Set-AzVirtualNetwork
$vnet = Get-AzVirtualNetwork -ResourceGroupName $rgName -Name $vnetname

if($vnet.Subnets.Count -eq 0) {
    Write-Host "===> Le sous-réseau n'a pas été créé"
    exit
}
else {
    Write-Host "===> Le sous-réseau" $vnet.Subnets.Name "a été créé"
    
}

if($vnet){
    Write-Host "===> Le réseau virtuel" $vnet.Name "a été créé"
}
else {
    Write-Host "===> Le réseau virtuel n'a pas été créé"
    exit
}

# Créer le NSG et ajouter la règle de sécurité
$nsg = @{
    Name = $NSGName
    ResourceGroupName = $rgName
    Location = $location
}
$nsg = New-AzNetworkSecurityGroup @nsg
$nsg = Get-AzNetworkSecurityGroup -ResourceGroupName $rgName -Name $NSGName

if ($nsg) {
    Write-Host "===> Le NSG a été créé"
}
else {
    Write-Host "===> Le NSG n'a pas été créé"
    exit
}

# Créer la règle de sécurité au NSG pour le port SSH
$ruleSSH = @{
    Name = "SSH"
    Protocol = "Tcp"
    Direction = "Inbound"
    Priority = 1000
    SourceAddressPrefix = "*"
    SourcePortRange = "*"
    DestinationAddressPrefix = "*"
    DestinationPortRange = 22
    Access = "Allow"
}

# Vérification de la création de la règle de sécurité
if ($ruleSSH) {
    Write-Host "===> La règle de sécurité a été créée"
}
else {
    Write-Host "===> La règle de sécurité n'a pas été créée"
    exit
}

# Créer la règle de sécurité au NSG
$nsg | Add-AzNetworkSecurityRuleConfig @ruleSSH | Set-AzNetworkSecurityGroup
$getNSG = Get-AzNetworkSecurityGroup -ResourceGroupName $rgName -Name $NSGName

# Adresse IP Publique de l'interface réseau
$ip = @{
	Name = $projectName + '-ip'
	ResourceGroupName = $rgName
	Location = $location
	Sku = 'Standard'
	AllocationMethod = 'Static'
    IpAddressVersion = 'IPv4'
	Zone = 2
	IdleTimeoutInMinutes = 4
}

$ip = New-AzPublicIpAddress @ip 
$getIp = Get-AzPublicIpAddress -ResourceGroupName $rgName -Name ($projectName + '-ip')

if ($getIp) {
    Write-Host "===> L'adresse IP publique est : " $getIp.IpAddress
}
else {
    Write-Host "===> L'adresse IP publique n'a pas été créée"
    exit
}

# Création de l'interface réseau + ajout du NSG à l'interface réseau
$nic = @{
    Name = $masterNICName
    ResourceGroupName = $rgName
    Location = $location
    SubnetId = $vnet.Subnets[0].Id
    NetworkSecurityGroupId = $nsg.Id
    PublicIpAddressId = $ip.Id
}
$nic = New-AzNetworkInterface @nic
$nic = Get-AzNetworkInterface -ResourceGroupName $rgName -Name $masterNICName

if ($nic) {
    Write-Host "===> L'interface réseau" $nic.Name "a été crée"
}
else {
    Write-Host "===> L'interface réseau n'a pas été créée"
    exit
}
$nic = Get-AzNetworkInterface -ResourceGroupName $rgName -Name $masterNICName

# Génération de la clé SSH (assurez-vous d'avoir les droits nécessaires pour créer la clé SSH)
## Essaie de création d'un fichier dans le dossier local .ssh

if (Test-Path $dossierSSH) {
    Write-Host "===> Le dossier .ssh existe"
    Write-Host "===> Création de la clé SSH"
    ssh-keygen -t rsa -b 4096 -C "Key for azureuser" -f $dossierSSH/"$projectName-sshKey" -q -N ""

    # Vérification de la clé SSH
    if (Test-Path $dossierSSH/"$projectName-sshKey") {
        Write-Host "===> La clé SSH a été créée"
    }   
    else {
        Write-Host "===> La clé SSH n'a pas été créée"
    }       
}
else {

    Write-Host "===> Le dossier .ssh n'existe pas"
    # Créer le dossier
    Write-Host "==> Création du dossier .ssh"
    New-Item -Path $dossierSSH -ItemType Directory -Force

    # Rendre le dossier caché sur Windows
    Set-ItemProperty -Path $dossier -Name Attributes -Value "Hidden"

    # Vérification de la création du dossier
    if (Test-Path $dossierSSH) {
        Write-Host "===> Le dossier .ssh a été créé"
        Write-Host "===> Création de la clé SSH"
        ssh-keygen -t rsa -b 4096 -C "Key for azureuser" -f $dossierSSH/"$projectName-sshKey" -q -N ""
        Write-Host "===> La clé SSH créée est : " $dossierSSH/"$projectName-sshKey"
    }
    else {
        Write-Host "===> Le dossier .ssh n'a pas pu été créé"
        # Demande à l'utilisateur de continuer le déploiement
        $reponse = Read-Host "La connexion se fera sans clé SSH. Voulez-vous continuer le déploiement ? (O/N)"
        if ($reponse -eq "O") {
            Write-Host "===> Le déploiement continue"
            # Ajout de temps pour que l'utilisateur puisse lire le message
            Start-Sleep -s 5
        }
        else {
            Write-Host "===> Le déploiement s'arrête"
            exit
        }
    }
}

<# # Lecture de la clé publique SSH
$sshKey = Get-Content ~/.ssh/"$projectName-sshKey".pub #>

#Compte utilisateur de la VM
$adminUsername = "azureuser" 
$adminUsername
$adminPassword = ConvertTo-SecureString MONmotdePASSE23+ -AsPlainText -Force
$credential = New-Object System.Management.Automation.PSCredential($adminUsername, $adminPassword) 

# VM 
$vmConfig = New-AzVMConfig -VMName $masterVMName -VMSize $VMSize -Zone 2 
Set-AzVMOperatingSystem -Linux -VM $vmConfig -ComputerName "TutoAnsible-server" -Credential $credential
Set-AZVMOSDisk -VM $vmConfig -Name $masterOSDiskName -DiskSizeInGB 30 -CreateOption FromImage
Set-AzVMSourceImage -VM $vmConfig -PublisherName $PublisherName -Offer $Offer -Skus $SKU -Version $version
Add-AzVMNetworkInterface -VM $vmConfig -Id $nic.Id
New-AzVM -ResourceGroupName $rgName -Location $location -VM $vmConfig -Verbose

$newVmName = $vmConfig.Name
$newVmSize = $vmConfig.VmSize

$vm = Get-AzVM -ResourceGroupName $rgName -Name $masterVMName

# vérification de la création de la VM
if ($vm) {
    Write-Host "La VM a été créée"
 }
 else {
     Write-Host "La VM n'a pas été créée"
     exit
 }

# Afficher les informations sur le réseau virtuel
Write-Host "***************************************"
Write-Host "Le réseau virtuel est : " $vnet.Name
Write-Host "L'id du réseau virtuel est : " $vnet.Id
Write-Host "L'adresse préfixe du réseau virtuel est : " $vnet.AddressSpace.AddressPrefix
Write-Host "***************************************"

# Afficher les informations sur le sous-réseau    
Write-Host "***************************************"
Write-Host "Le nom du sous-réseau est : " $vnet.Subnets.Name
Write-Host "L'id du sous-réseau est : " $vnet.Subnets.Id
Write-Host "L'adresse préfixe du sous-réseau est : " $vnet.Subnets.AddressPrefix 
Write-Host "***************************************"   

# Afficher les informations sur le NSG
Write-Host "***************************************"
Write-Host "Le nom du NSG est : " $nsg.Name
Write-Host "L'id du NSG est : " $nsg.Id
Write-Host "***************************************"

# Afficher les informations sur l'interface réseau
Write-Host "***************************************"
Write-Host "Le nom de l'interface réseau est : " $nic.Name
Write-Host "L'id de l'interface réseau est : " $nic.Id
Write-Host "L'id de l'adresse IP publique est : " $nic.IpConfigurations.PublicIpAddress.Id
Write-Host "***************************************"

# Afficher les informations sur la VM
Write-Host "***************************************"
Write-Host "Le nom de la VM est : " $vm.Name
Write-Host "L'id de la VM est : " $vm.Id
Write-Host "L'adresse IP publique de la VM est : " $getIp.IpAddress
Write-Host "L'adresse IP privée de la VM est : " $nic.IpConfigurations.PrivateIpAddress
Write-Host "***************************************"

# rajouter du temps pour que l'utilisateur puisse lire les informations 
Start-Sleep -s 5

# afficher un compte à rebours animé avant le démarrage de la VM 
for ($i = 5; $i -ge 0; $i--) {
    Write-Host "Démarrage de la VM dans " $i " secondes"
    Start-Sleep -s 1
}

# Démarrer la VM
Write-Host "***************************************"
Write-Host "*********Démarrage la VM***************"
Write-Host "***************************************"
Start-AzVM -ResourceGroupName $rgName -Name $masterVMName

# Installation du module Az.Ssh
Write-Host "***************************************"
Write-Host "Installation du module Az.Ssh"
Write-Host "***************************************"
Install-Module -Name Az.Ssh -AllowClobber -Scope CurrentUser


$vm = Get-AzVM -ResourceGroupName $rgName -Name $masterVMName

Get-AzVM -ResourceGroupName "TutoAnsible-rg" -Name "TutoAnsiblemaster-vm"



# Récupérer l'adresse IP de la VM
$ip = $getIp.IpAddress

# Ajouter la clé SSH à la VM
## Vérification de l'existence de la clé SSH
if (Test-Path $dossierSSH\"$projectName-sshKey.pub") {
    scp $HOME/.ssh/"$projectName-sshKey.pub" $adminUsername@$ip':~/.ssh/'$projectName'-sshKey.pub'
    Write-Host "===> La clé SSH existe en local, elle a été ajoutée à la VM via SCP"
    Get-Content $dossierSSH\"$projectName-sshKey.pub" | ssh -i $dossierSSH/"$projectName-sshKey" $adminUsername@$ip "cat >> ~/.ssh/authorized_keys" 
    Write-Host "===> Les informations de la clé SSH existent, elles ont été ajoutées à la VM via SSH"
}
else {
    Write-Host "===> La clé SSH n'existe pas ou ses informations n'éxistent pas, elle n'a pas été ajoutée à la VM"
}

# Installation de d'Ansible sur la VM
Write-Host "***************************************"
Write-Host "Installation d'Ansible sur la VM"
Write-Host "***************************************"

Set-AzVMExtension -ResourceGroupName $rgName -VMName $masterVMName -Name "installAnsible" -Publisher "Microsoft.Azure.Extensions" -Type "CustomScript" -TypeHandlerVersion 2.0 -SettingString '{"commandToExecute":"apt-get update && apt-get install -y software-properties-common && apt-add-repository --yes --update ppa:ansible/ansible && apt-get install -y ansible"}' -Location $location
Update-AzVM -VM $vm -ResourceGroupName $rgName

# Vérification de l'installation d'Ansible
$vm = Get-AzVM -ResourceGroupName $rgName -Name $masterVMName

if ($vm.Extensions[0].ProvisioningState -eq "Succeeded") {
    Write-Host "***************************************"
    Write-Host "Ansible a été installé sur la VM"
    Write-Host "***************************************"
}
else {
    Write-Host "***************************************"
    Write-Host "Ansible n'a pas été installé sur la VM"
    Write-Host "***************************************"
}

# Affichage de la version d'Ansible de la VM
Write-Host "***************************************"
Write-Host "La version d'Ansible de la VM est :"
Invoke-AzVMRunCommand -ResourceGroupName $rgName -VMName $masterVMName -CommandId "RunShellScript" -Script "ansible --version" -Verbose
Write-Host "***************************************"


# Connexion à la VM
if (Test-Path $dossierSSH/"$projectName-sshKey" && Test-Path $dossierSSH/"$projectName-sshKey.pub") {
    Write-Host "***************************************"
    Write-Host "Connexion à la VM avec la clé SSH"
    Write-Host "***************************************"
    Enter-AzVM -ResourceGroupName $rgName -Name $masterVMName -LocalUser $adminUsername -PublicKeyFile ~/.ssh/"$projectName-sshKey.pub" -PrivateKeyFile ~/.ssh/"$projectName-sshKey" -Verbose
}
else {
    Write-Host "***************************************"
    Write-Host "Connexion à la VM avec le mot de passe"
    Write-Host "***************************************"
    Enter-AzVM -ResourceGroupName $rgName -Name $masterVMName -LocalUser $adminUsername -Password $adminPassword -Verbose
}
<#********************************************************************************************************************

# Arrêt de la VM
Write-Host "Arrêt de la VM"
Stop-AzVM -ResourceGroupName $rgName -Name $masterVMName

# Suppression du groupe de ressources
Write-Host "Suppression du groupe de ressources"
Remove-AzResourceGroup -Name $rgName -Force -Verbose
```
********************************************************************************************************************#>